#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include "input.h"


void run_rot_13_cipher( void )
{

	int choice;
	char* phrase = (char*)malloc(1000); // memory allocation (C)
	// char* phrase = new char[1000]; // memory allocation (C++)
	int i;
	int j;
	int r;
	int e = 0;
	int q;
	int b = 0;
	int diff;
	int diff2;
	int diff3;
	int stop;

	char* tmp = (char*)malloc(1000); // memory allocation (C)

	typedef struct
	{
		char x;
		char y;
		int place;
	}assign_table;


	srand(time(NULL));
	assign_table upper_assign[] = {
		{'A','N',0},
		{'B','O',1},
		{'C','P',2},
		{'D','Q',3},
		{'E','R',4},
		{'F','S',5},
		{'G','T',6},
		{'H','U',7},
		{'I','V',8},
		{'J','W',9},
		{'K','X',10},
		{'L','Y',11},
		{'M','Z',12},
		{'N','A',13},
		{'O','B',14},
		{'P','C',15},
		{'Q','D',16},
		{'R','E',17},
		{'S','F',18},
		{'T','G',19},
		{'U','H',20},
		{'V','I',21},
		{'W','J',22},
		{'X','K',23},
		{'Y','L',24},
		{'Z','M',25}
	};
	assign_table lower_assign[] = {
		{'a','n',0},
		{'b','o',1},
		{'c','p',2},
		{'d','q',3},
		{'e','r',4},
		{'f','s',5},
		{'g','t',6},
		{'h','u',7},
		{'i','v',8},
		{'j','w',9},
		{'k','x',10},
		{'l','y',11},
		{'m','z',12},
		{'n','a',13},
		{'o','b',14},
		{'p','c',15},
		{'q','d',16},
		{'r','e',17},
		{'s','f',18},
		{'t','g',19},
		{'u','h',20},
		{'v','i',21},
		{'w','j',22},
		{'x','k',23},
		{'y','l',24},
		{'z','m',25}

	};

	do{
		printf("Welcome To Rot 13 Cipher!\n(1)Encryption\n(2)Decryption\n(3)Quit\n\n");

		choice = input_number(1,3);

		if(choice == 1){

			while(getchar() != '\n');

			printf("Enter Phrase to be Encrypted: ");
			size_t n = 1000;
			getline(&phrase, &n, stdin);

			size_t len = strlen(phrase);
			for(i = 0;i < len; i++){
				for(j = 0;j < 26; j++){
					if(phrase[i] == upper_assign[j].x){
						tmp[i] = upper_assign[j].y;
						break;
					}else if(phrase[i] == lower_assign[j].x){
						tmp[i] = lower_assign[j].y;
						break;
					}else if(j == 25){
						tmp[i] = phrase[i];
					}
				}
			}
			printf("\nMessage: ");
			for(i=0;i<len;i++){
				printf("%c",tmp[i]);
			}
			printf("\n");
			sleep(2);

		}else if(choice == 2){

			while(getchar() != '\n');

                        printf("Enter Phrase to be Decrypted: ");
                        size_t n = 1000;
                        getline(&phrase, &n, stdin);

                        size_t len = strlen(phrase);
                        for(i = 0;i < len; i++){
                                for(j = 0;j<26;j++){
                                        if(phrase[i] == upper_assign[j].x){
                                                tmp[i] = upper_assign[j].y;
                                                break;
                                        }else if(phrase[i] == lower_assign[j].x){
                                                tmp[i] = lower_assign[j].y;
                                                break;
                                        }else if(j == 25){
                                                tmp[i] = phrase[i];
                                        }
                                }
                        }
			printf("\nMessage: ");
                        for(i=0;i<len;i++){
                                printf("%c",tmp[i]);
                        }
			printf("\n");
			sleep(2);

		}else if(choice == 3){
			b = 1;
		}

	}while(b == 0);

}

