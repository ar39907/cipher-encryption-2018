#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <stdio.h>
#include "input.h"

void run_a1z26( void )
{
	int i;
	int j = 4;
	int p;
	int b = 0;
	int choice;
	char* phrase = (char*)malloc(1000);

	typedef struct key_pair {
		char x;
		int y;
	} key_t;

	key_t pairs[52] = {
		{'A',1 },
		{'B',2 },
		{'C',3 },
		{'D',4 },
		{'E',5 },
		{'F',6 },
		{'G',7 },
		{'H',8 },
		{'I',9 },
		{'J',10 },
		{'K',11 },
		{'L',12 },
		{'M',13 },
		{'N',14 },
		{'O',15 },
		{'P',16 },
		{'Q',17 },
		{'R',18 },
		{'S',19 },
		{'T',20 },
		{'U',21 },
		{'V',22 },
		{'W',23 },
		{'X',24 },
		{'Y',25 },
		{'Z',26 },
                {'a',1 },
                {'b',2 },
                {'c',3 },
                {'d',4 },
                {'e',5 },
                {'f',6 },
                {'g',7 },
                {'h',8 },
                {'i',9 },
                {'j',10 },
                {'k',11 },
                {'l',12 },
                {'m',13 },
                {'n',14 },
                {'o',15 },
                {'p',16 },
                {'q',17 },
                {'r',18 },
                {'s',19 },
                {'t',20 },
                {'u',21 },
                {'v',22 },
                {'w',23 },
                {'x',24 },
                {'y',25 },
                {'z',26 },

	};

	do{

//		printf("Welcome to A1Z26 Cipher!\n(1)Encryption\n(2)Decryption\n(3)Quit\n\n");
//		scanf("%d",&choice);


			printf("Welcome to A1Z26 Cipher!\n(1)Encryption\n(2)Decryption\n(3)Quit\n\n");

			choice = input_number(1,3);

			if(choice == 1){

			printf("Enter Phrase: ");


			while(getchar() != '\n');

			size_t n = 1000;
			getline(&phrase, &n, stdin);

			size_t len = strlen(phrase);

			int tmp[len];
			for(i=0;i<len;i++){
				tmp[i] = 0;
			}
			char tmpchars[len];
			for(i=0;i<len;i++){
				tmpchars[i] = '0';
			}
			for(i=0;i<len;i++){
				for(j=0;j<52;j++){
					if(phrase[i] == pairs[j].x){
						tmp[i] = pairs[j].y;
						break;
					}
					if(j == 51){
						tmpchars[i] = phrase[i];
					}
				}
			}
			printf("\nMessage: ");
			for(i=0;i<len;i++){

				if(tmp[i] == 0){
					printf("%c",tmpchars[i]);
				}else{
					printf("%d",tmp[i]);

					if(tmp[i+1] != 0){
						printf("-");
					}

				}

			}
			printf("\n");
			sleep(2);

		}else if(choice == 2){

			printf("Enter A1Z26 Code (20-8-9-19 9-19 1 20-5-19-20): ");

			while(getchar() != '\n');

			size_t n = 1000;
			getline(&phrase, &n, stdin);

			size_t len = strlen(phrase);

			int prev_str = 0;
			int w = 0;
			int cnt = 0;
			char tmp[len*2];
			for(i = 0;i<len*2;i++){
				tmp[i] = '0';
			}
			for(i = 0;i<len*2;i++){
					if(phrase[i] < '0' || phrase[i] > '9'){
						if(w == 0){
							tmp[cnt] = phrase[i];
							cnt = cnt + 1;
						}else{
							for(p=0;p<52;p++){
								if(pairs[p].y == w){
									tmp[cnt] = pairs[p].x;
									cnt = cnt + 1;
									w = 0;
									if(phrase[i] != '-'){
										tmp[cnt] = phrase[i];
										cnt = cnt + 1;
									}
									break;
								}
							}
						}
					}else{
						if(phrase[i+1] >= '0' && phrase[i+1] <= '9'){
							w = w + ((phrase[i]-48)*10);
						}else{
							w = w + (phrase[i]-48);
						}
					}

			}

			printf("\nMessage: ");
			for(i=0;i<len*2;i++){
				if(tmp[i] != '0'){
					printf("%c",tmp[i]);
				}
			}
			printf("\n");
			sleep(2);
		}else if(choice == 3){
			b = 1;
		}
	}while(b == 0);

}
