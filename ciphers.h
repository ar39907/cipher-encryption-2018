
#ifndef _ciphers_h
#define _ciphers_h

void run_a1z26( void );
void run_caesar_cipher( void );
void run_rail_fence_cipher( void );
void run_rand_assign( void );
void run_rot_13_cipher( void );

#endif

