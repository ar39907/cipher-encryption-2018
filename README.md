# Cypher-Encryption-2018



## Description

Archived cypher project from 2018.


## Getting Started

### Dependencies

* C Standard Library, GCC
* Windows


### Usage

* Compile by running compile.h
* Run executable


## Author

Austin Richie

https://gitlab.com/ar39907/
