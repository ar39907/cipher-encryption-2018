#include <math.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include "input.h"

void run_rand_assign( void )
{
	typedef struct
	{
		char x;
		char y;
		int used;
	}assign_table;

	int choice;
	char* phrase = (char*)malloc(1000); // memory allocation (C)
	// char* phrase = new char[1000]; // memory allocation (C++)
	int i;
	int j;
	int r;
	int q;
	int p = 0;
	int b = 0;
	char* key = (char*)malloc(1000); // memory allocation (C)

	char* tmp = (char*)malloc(1000); // memory allocation (C)

	srand(time(NULL));
	assign_table upper_assign[] = {
		{'A','A',0},
		{'B','A',0},
		{'C','A',0},
		{'D','A',0},
		{'E','A',0},
		{'F','A',0},
		{'G','A',0},
		{'H','A',0},
		{'I','A',0},
		{'J','A',0},
		{'K','A',0},
		{'L','A',0},
		{'M','A',0},
		{'N','A',0},
		{'O','A',0},
		{'P','A',0},
		{'Q','A',0},
		{'R','A',0},
		{'S','A',0},
		{'T','A',0},
		{'U','A',0},
		{'V','A',0},
		{'W','A',0},
		{'X','A',0},
		{'Y','A',0},
		{'Z','A',0},
	};
	assign_table lower_assign[] = {
		{'a','A',0},
		{'b','A',0},
		{'c','A',0},
		{'d','A',0},
		{'e','A',0},
		{'f','A',0},
		{'g','A',0},
		{'h','A',0},
		{'i','A',0},
		{'j','A',0},
		{'k','A',0},
		{'l','A',0},
		{'m','A',0},
		{'n','A',0},
		{'o','A',0},
		{'p','A',0},
		{'q','A',0},
		{'r','A',0},
		{'s','A',0},
		{'t','A',0},
		{'u','A',0},
		{'v','A',0},
		{'w','A',0},
		{'x','A',0},
		{'y','A',0},
		{'z','A',0}
	};

	do{
			printf("Mixed Alphabet Cipher\n(1)Encryption\n(2)Decryption\n(3)Quit\n\n");

			choice = input_number(1,3);

			if(choice == 1){

				printf("(1)Automatic Key\n(2)Manual Key\n\n");

				choice = input_number(1,2);

				if(choice == 1){
					for(i=0;i<26;i++){
						do {
							r = rand() % 26;
							if(upper_assign[r].used == 1 && lower_assign[r].used == 1){
								q = 2;
							}else if(upper_assign[r].used == 0 && lower_assign[r].used == 0){
								upper_assign[i].y = upper_assign[r].x;
								lower_assign[i].y = lower_assign[r].x;
								upper_assign[r].used = 1;
								lower_assign[r].used = 1;
								q = 1;
							}

						} while(q==2);
					}


					printf("View Random Key? You will need it for decryption\n(1)Yes\n(2)No\n\n");

					choice = input_number(1,2);
					if(choice == 1){
						for(i=0;i<26;i++){
							printf("%c = %c\n",upper_assign[i].x,upper_assign[i].y);
						}
						printf("Copy this key in order to use it for decryption: ");
						for(i=0;i<26;i++){
							printf("%c",upper_assign[i].y);
						}
						printf("\n");
					}

				}else if(choice == 2){

					printf("Enter Key of all 26 letters in any order (Ex: ZASXDCFVGBHNJMKLQWERTYUIOP): \n");
					q = 2;
					while(q==2){

						scanf("%s",phrase);
						size_t len = strlen(phrase);

						if(len > 26){
							printf("Too Many Letters. Try Again.\n");
							q = 2;
						}else if(len < 26){
							printf("Not Enough Letters. Try Again.\n");
							q = 2;
						}else{
							for(i=0;i<len;i++){
								for(j=0;j<26;j++){
									if(phrase[i] == upper_assign[j].x){
										upper_assign[j].used = 1;
									}
								}
							}
							for(i=0;i<26;i++){
								if(upper_assign[i].used == 1){
									p = p + 1;
								}
							}
							if(p == 26){ //this means the string works and is valid
								for(i=0;i<26;i++){
									upper_assign[i].y = phrase[i];
									lower_assign[i].y = phrase[i]+32;
									q = 1;


								}
							}else{
								printf("Try Again (use each letter once)\n");
							}
						}



					};


				}

				while(getchar() != '\n');
				printf("Enter Phrase to be Encrypted: ");

				size_t n = 1000;
				getline(&phrase, &n, stdin);

				size_t len = strlen(phrase);
				for(i = 0;i < len; i++){
					for(j = 0;j<26;j++){
						if(phrase[i] == upper_assign[j].x){
							tmp[i] = upper_assign[j].y;
							break;
						}else if(phrase[i] == lower_assign[j].x){
							tmp[i] = lower_assign[j].y;
							break;
						}else if(j == 25){
							tmp[i] = phrase[i];
						}
					}
				}
				printf("\nMessage: ");
				for(i=0;i<len;i++){
					printf("%c",tmp[i]);
				}

				printf("\n");
				sleep(2);

			}else if(choice == 2){

				printf("Enter Key of all 26 letters in any order (Ex: ZASXDCFVGBHNJMKLQWERTYUIOP): \n");

				q = 2;
				while(q==2){

					scanf("%s",phrase);
					size_t len = strlen(phrase);

					if(len > 26){
						printf("Too Many Letters. Try Again.\n");
						q = 2;
					}else if(len < 26){
						printf("Not Enough Letters. Try Again.\n");
						q = 2;
					}else{
						for(i=0;i<len;i++){
							for(j=0;j<26;j++){
								if(phrase[i] == upper_assign[j].x){
									upper_assign[j].used = 1;
								}
							}
						}
						for(i=0;i<26;i++){
							if(upper_assign[i].used == 1){
								p = p + 1;
							}
						}
						if(p == 26){ //this means the string works and is valid
							for(i=0;i<26;i++){
								upper_assign[i].y = phrase[i];
								lower_assign[i].y = phrase[i]+32;
								q = 1;


							}
						}else{
							printf("Try Again (use each letter once)\n");
						}
					}

				};


				while(getchar() != '\n');
				printf("Enter Phrase to be Decrypted: ");

				size_t n = 1000;
				getline(&phrase, &n, stdin);

				size_t len = strlen(phrase);
				for(i = 0;i < len; i++){
					for(j = 0;j<26;j++){
						if(phrase[i] == upper_assign[j].y){
							tmp[i] = upper_assign[j].x;
							break;
						}else if(phrase[i] == lower_assign[j].y){
							tmp[i] = lower_assign[j].x;
							break;
						}else if(j == 25){
							tmp[i] = phrase[i];
						}
					}
				}
				printf("\nMessage: ");

	                        for(i=0;i<len;i++){
	                                printf("%c",tmp[i]);
	                        }
				printf("\n");
				sleep(2);

			}else if(choice == 3){
				b = 1;
			}

	}while(b == 0);


}
