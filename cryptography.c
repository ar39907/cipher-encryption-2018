#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include "input.h"
#include "ciphers.h"

int main()
{
	int choice = 0;
	int b = 0;

	do{

		do{
			printf(
				"Welcome to the Cryptography Application\n"
				"(1)A1Z26 Cipher\n"
				"(2)Caesar Cipher\n"
				"(3)Rail Fence Cipher\n"
				"(4)Random Assignment Cipher\n"
				"(5)Rot 13 Cipher\n"
				"(6)Quit\n\n");

			choice = input_number(1,6);

		}while(choice < 1 || choice > 6);


		if(choice == 1){

			run_a1z26();

		}else if(choice == 2){

			run_caesar_cipher();

		}else if(choice == 3){

			run_rail_fence_cipher();

		}else if(choice == 4){

			run_rand_assign();

		}else if(choice == 5){

			run_rot_13_cipher();

		}else if(choice == 6){

			b = 1;

		}

	}while(b == 0);
}
