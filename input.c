#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "input.h"

int input_number( int min_val, int max_val )
{
	size_t n = 0;
	char* buf;
	int choice;
	int b;
	int i;
	int num_of_attempts = 10;

	for(i=0;i<num_of_attempts;i++){

		printf("] ");

//		while(getchar() != '\n');

		getline(&buf, &n, stdin);

		choice = atoi(buf);

		if(choice >= min_val && choice <= max_val){
			return choice;
		}

		printf("Error: Invalid Input\n");
	}
	printf("Sorry, after 10 tries I am exiting\n");
	exit(-1);

}
