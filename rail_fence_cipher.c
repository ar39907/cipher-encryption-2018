#include <time.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include "input.h"

void run_rail_fence_cipher( void )
{
	typedef struct
	{
		char x;
		char y;
		int used;
	}assign_table;

	int choice;
	char* phrase = (char*)malloc(1000);
	int i;
	int j;
	int r;
	int b = 0;
	int q;
	int z;
	int count = 0;
	int cnt = 0;
	char* tmp = (char*)malloc(1000);

	srand(time(NULL));
	assign_table upper_assign[] = {
		{'A','A',0},
		{'B','A',0},
		{'C','A',0},
		{'D','A',0},
		{'E','A',0},
		{'F','A',0},
		{'G','A',0},
		{'H','A',0},
		{'I','A',0},
		{'J','A',0},
		{'K','A',0},
		{'L','A',0},
		{'M','A',0},
		{'N','A',0},
		{'O','A',0},
		{'P','A',0},
		{'Q','A',0},
		{'R','A',0},
		{'S','A',0},
		{'T','A',0},
		{'U','A',0},
		{'V','A',0},
		{'W','A',0},
		{'X','A',0},
		{'Y','A',0},
		{'Z','A',0},
	};
	assign_table lower_assign[] = {
		{'a','A',0},
		{'b','A',0},
		{'c','A',0},
		{'d','A',0},
		{'e','A',0},
		{'f','A',0},
		{'g','A',0},
		{'h','A',0},
		{'i','A',0},
		{'j','A',0},
		{'k','A',0},
		{'l','A',0},
		{'m','A',0},
		{'n','A',0},
		{'o','A',0},
		{'p','A',0},
		{'q','A',0},
		{'r','A',0},
		{'s','A',0},
		{'t','A',0},
		{'u','A',0},
		{'v','A',0},
		{'w','A',0},
		{'x','A',0},
		{'y','A',0},
		{'z','A',0}
	};

	do{

		printf("Welcome to Rail Fence Cipher\n(1)Encryption\n(2)Decryption\n(3)Quit\n\n");

		choice = input_number(1,3);

		if(choice == 1){


			printf("(1)Automatic/Random Key\n(2)Manual Key\n\n");

			choice = input_number(1,2);

			if(choice == 1){

				r = rand() % 4 + 2;

			//Random Key

				printf("View Random Key?\n(1)Yes\n(2)No\n\n");

				choice = input_number(1,2);

				if(choice == 1){

					printf("Key=%d\n",r);

				}else if(choice == 2){

				}


			}else if(choice == 2){
				r = 2;
				printf("Enter Number for Key (Ex:4): ");

				choice = input_number(1,10);
			}

			while(getchar() != '\n');

			printf("Enter Phrase to be Encrypted: ");
			size_t n = 1000;
			getline(&phrase, &n, stdin);



			size_t len = strlen(phrase);
			int a[1000];

			for(i=0;i<len;i++){
				if(phrase[i] == ' '){
					phrase[i] = '_';
				}
			}
			for(i=0; i<len; i++){
				for(j=0;j<r;j++){
					a[cnt] = j;
					cnt = cnt + 1;
				}
				for(j=r-2;j>0;j--){
					a[cnt] = j;
					cnt = cnt + 1;
				}
			}
			cnt = 0;

			for(i = 0;i < r;i++){
				for(j = 0;j < len;j++){
					if(a[j]==i){
						tmp[cnt] = phrase[j];
						cnt = cnt + 1;
					}
				}
			}
			printf("\nMessage: ");
			for(i=0;i<len;i++){
				if(tmp[i] != '\n'){
					printf("%c",tmp[i]);
				}
			}
			printf("\n");
			printf("\n");
			sleep(2);

		}else if(choice == 2){

			printf("Enter Number for Key(Ex:4): ");

			choice = input_number(1,10);

			while(getchar() != '\n');

			printf("Enter Phrase to be Decrypted: ");
			size_t n = 1000;
			getline(&phrase, &n, stdin);
			size_t len = strlen(phrase) - 1;
			int a[1000];

			for(i=0;i<len;i++){
				if(phrase[i] == ' '){
					phrase[i] = '_';
				}

			}

			for(i=0; i<len/r; i++){
				for(j=0;j<r;j++){
					a[cnt] = j;
					cnt = cnt + 1;
				}
				for(j=r-2;j>0;j--){
					a[cnt] = j;
					cnt = cnt + 1;
				}
			}

			int d[r];
			for(i=0;i<r;i++){
				d[i] = 0;
			}
			for(i=0; i<len; i++){
				for(j=0; j<r; j++){
					if(a[i] == j){
						d[j] = d[j] + 1;
						break;
					}
				}
			}
			char row[r][len];
			cnt = 0;
			for(i=0;i<r;i++){
				for(j=0;j<d[i];j++){
					row[i][j] = phrase[cnt];
					cnt = cnt + 1;
				}
			}
			cnt = 0;
			int r_count[r];
			for(i=0;i<r;i++){
				r_count[i] = 0;
			}
			while(cnt<len){
				for(i=0;i<r;i++){
					if(cnt < len){
						tmp[cnt] = row[i][r_count[i]];
						r_count[i] = r_count[i] + 1;
						cnt = cnt + 1;
					}
				}

				for(i=r-2;i>0;i--){
					if(cnt < len){
						tmp[cnt] = row[i][r_count[i]];
						r_count[i] = r_count[i] + 1;
						cnt = cnt + 1;
					}
				}
			};
			printf("\nMessage: ");
			for(i=0;i<len;i++){
				if(tmp[i] != '\n'){
					printf("%c",tmp[i]);
				}
			}
			printf("\n");
			printf("\n");
			sleep(2);

		}else if(choice == 3){
			b = 1;
		}



	}while(b == 0);
}
