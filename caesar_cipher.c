#include <unistd.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdio.h>
#include "input.h"

void run_caesar_cipher( void )
{


	typedef struct
	{
		char x;
		char y;
		int place;
	}assign_table;


	int choice;
	char* phrase = (char*)malloc(1000); // memory allocation (C)
	// char* phrase = new char[1000]; // memory allocation (C++)
	int i;
	int j;
	int r;
	int e = 0;
	int q;
	int b = 0;
	int diff;
	int diff2;
	int diff3;
	int stop;

	char* tmp = (char*)malloc(1000); // memory allocation (C)

	srand(time(NULL));
	assign_table upper_assign[] = {
		{'A','A',0},
		{'B','A',1},
		{'C','A',2},
		{'D','A',3},
		{'E','A',4},
		{'F','A',5},
		{'G','A',6},
		{'H','A',7},
		{'I','A',8},
		{'J','A',9},
		{'K','A',10},
		{'L','A',11},
		{'M','A',12},
		{'N','A',13},
		{'O','A',14},
		{'P','A',15},
		{'Q','A',16},
		{'R','A',17},
		{'S','A',18},
		{'T','A',19},
		{'U','A',20},
		{'V','A',21},
		{'W','A',22},
		{'X','A',23},
		{'Y','A',24},
		{'Z','A',25},
	};
	assign_table lower_assign[] = {
		{'a','A',0},
		{'b','A',1},
		{'c','A',2},
		{'d','A',3},
		{'e','A',4},
		{'f','A',5},
		{'g','A',6},
		{'h','A',7},
		{'i','A',8},
		{'j','A',9},
		{'k','A',10},
		{'l','A',11},
		{'m','A',12},
		{'n','A',13},
		{'o','A',14},
		{'p','A',15},
		{'q','A',16},
		{'r','A',17},
		{'s','A',18},
		{'t','A',19},
		{'u','A',20},
		{'v','A',21},
		{'w','A',22},
		{'x','A',23},
		{'y','A',24},
		{'z','A',25}
	};

	do{
		printf("Welcome To Caesar Cipher!\n(1)Encryption\n(2)Decryption\n(3)Quit\n\n");

		choice = input_number(1,3);

		if(choice == 1){

			printf("(1)Automatic/Random Key\n(2)Manual Key\n\n");

			choice = input_number(1,2);

			if(choice == 1){

				r = rand() % 26;

			}else if(choice == 2){
				printf("Enter Shift Key (Ex: 5): ");

				choice = input_number(0,26);

			}
			diff = r;
			stop = 25-r;
			for(i=0;i<26;i++){
				if(upper_assign[i].place <= stop){
					upper_assign[i].y = upper_assign[i].x+diff;
					lower_assign[i].y = lower_assign[i].x+diff;
				}else if(upper_assign[i].place >= stop){
					diff2 = 25 - upper_assign[i].place;
					diff3 = diff-diff2;
					upper_assign[i].y = upper_assign[diff3-1].x;
					lower_assign[i].y = lower_assign[diff3-1].x;
				}
			}
			printf("View Key?\n(1)Yes\n(2)No\n\n");

			choice = input_number(1,2);

			if(choice == 1){
				printf("%c = %c\n",upper_assign[0].x,upper_assign[0].y);
			}

			while( getchar() != '\n');

			printf("Enter Phrase to be Encrypted: ");
//			scanf("%s",phrase);
			size_t n = 1000;
			getline(&phrase, &n, stdin);

			size_t len = strlen(phrase);
			for(i = 0;i < len; i++){
				for(j = 0;j<26;j++){
					if(phrase[i] == upper_assign[j].x){
						tmp[i] = upper_assign[j].y;
						break;
					}else if(phrase[i] == lower_assign[j].x){
						tmp[i] = lower_assign[j].y;
						break;
					}else if(j == 25){
						tmp[i] = phrase[i];
					}
				}
			}
			printf("\nMessage: ");
			for(i=0;i<len;i++){
				printf("%c",tmp[i]);
			}
			printf("\n");
			sleep(2);

		}else if(choice == 2){

			printf("Enter Shift Key (Ex: 5): ");

			choice = input_number(0,26);

                        diff = r;
                        stop = 25-r;
                        for(i=0;i<26;i++){
                                if(upper_assign[i].place <= stop){
                                        upper_assign[i].y = upper_assign[i].x+diff;
                                        lower_assign[i].y = lower_assign[i].x+diff;
                                }else if(upper_assign[i].place >= stop){
                                        diff2 = 25 - upper_assign[i].place;
                                        diff3 = diff-diff2;
                                        upper_assign[i].y = upper_assign[diff3-1].x;
                                        lower_assign[i].y = lower_assign[diff3-1].x;
                                }
                        }

			while( getchar() != '\n');

                        printf("Enter Phrase to be Decrypted: ");
//                      scanf("%s",phrase);
                        size_t n = 1000;
                        getline(&phrase, &n, stdin);

                        size_t len = strlen(phrase);
                        for(i = 0;i < len; i++){
                                for(j = 0;j<26;j++){
                                        if(phrase[i] == upper_assign[j].x){
                                                tmp[i] = upper_assign[j].y;
                                                break;
                                        }else if(phrase[i] == lower_assign[j].x){
                                                tmp[i] = lower_assign[j].y;
                                                break;
                                        }else if(j == 25){
                                                tmp[i] = phrase[i];
                                        }
                                }
                        }
			printf("\nMessage: ");
                        for(i=0;i<len;i++){
                                printf("%c",tmp[i]);
                        }
                        printf("\n");
                        sleep(2);





		}else if(choice == 3){
			b = 1;
		}

	}while(b == 0);


}
